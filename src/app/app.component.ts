import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { PaymentDetails } from 'src/models/PaymentDetails';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'payage-test';
  paymentDetails: PaymentDetails | undefined;
  selectedPaymentMethod!: number;

  constructor(private http: HttpClient, private sharedService: SharedService) {
    this.sharedService.selectedPaymentMethod.subscribe(paymentMethod => this.selectedPaymentMethod = paymentMethod);
    this.http.get<PaymentDetails>('http://localhost:4200/api/paymentDetails').subscribe(res => {
      this.paymentDetails = res;
    })
  }

}
