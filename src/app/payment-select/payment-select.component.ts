import { Component, Input, OnInit } from '@angular/core';
import { PaymentDetails } from 'src/models/PaymentDetails';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-payment-select',
  templateUrl: './payment-select.component.html',
  styleUrls: ['./payment-select.component.css']
})
export class PaymentSelectComponent implements OnInit {

  @Input() paymentDetails: PaymentDetails | undefined;

  constructor(private sharedService: SharedService) { }

  ngOnInit(): void { }

  paymentMethodSelected($event: any) {
    this.sharedService.changeSelectedPaymentMethod($event.value);
  }

}