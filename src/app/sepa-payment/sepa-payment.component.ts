import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Payment } from 'src/models/Payment';
import { PaymentDetails } from 'src/models/PaymentDetails';
import { PaymentMethodConfig } from 'src/models/PaymentMethodConfig';
import { PaymentMethodDetails } from 'src/models/PaymentMethodDetails';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-sepa-payment',
  templateUrl: './sepa-payment.component.html',
  styleUrls: ['./sepa-payment.component.css']
})
export class SepaPaymentComponent implements OnInit {

  paymentsForm!: FormGroup;
  formConfig: PaymentMethodConfig[] = [];
  submitted: boolean = false;
  @Input() paymentDetails!: PaymentDetails;
  selectedPaymentMethod!: number;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private sharedService: SharedService) { }

  ngOnInit(): void {
    this.sharedService.selectedPaymentMethod.subscribe(paymentMethod => this.selectedPaymentMethod = paymentMethod);
    this.http.get<PaymentMethodDetails>('http://localhost:4200/api/paymentMethodDetails/' + this.selectedPaymentMethod).subscribe(res => {
      this.formConfig = res.configuration;
    })
    this.paymentsForm = this.formBuilder.group({
      iban: ['', Validators.compose([Validators.required, Validators.minLength(15), Validators.maxLength(32), Validators.pattern(/^$|^[a-zA-Z0-9]+$/)])],
      bic: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(8), Validators.pattern(/^$|^[a-zA-Z0-9]+$/)])],
      holderName: ['', Validators.compose([Validators.required, Validators.minLength(15), Validators.maxLength(50)])],
      payments: this.formBuilder.array([])
    })
  }

  submit() {
    this.submitted = true;
    if (this.paymentsForm.valid) {

      this.http.get<Payment[]>('http://localhost:4200/api/payments').subscribe(res => {
        let payment = new Payment(res.length + 1, this.selectedPaymentMethod, this.paymentDetails.amount, this.paymentDetails.currency,
          this.paymentsForm.controls.iban?.value, this.paymentsForm.controls.bic?.value,
          this.paymentsForm.controls.holderName?.value, this.paymentsForm.controls.cardNumber?.value,
          this.paymentsForm.controls.expDate?.value, this.paymentsForm.controls.cvc?.value, this.paymentsForm.controls.cardHolder?.value);

        this.http.post<Payment>('http://localhost:4200/api/payments', payment).subscribe(res => {
          this.http.get<Payment[]>('http://localhost:4200/api/payments').subscribe(res => {
            console.log(res);
            this.sharedService.changeSelectedPaymentMethod(3);
          })
        })
      })

    }
  }
  
  returnToPaymentSelect() {
    this.sharedService.changeSelectedPaymentMethod(0);
  }

}
