import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Payment } from 'src/models/Payment';
import { PaymentDetails } from 'src/models/PaymentDetails';
import { PaymentMethodConfig } from 'src/models/PaymentMethodConfig';
import { PaymentMethodDetails } from 'src/models/PaymentMethodDetails';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-visa-payment',
  templateUrl: './visa-payment.component.html',
  styleUrls: ['./visa-payment.component.css']
})
export class VisaPaymentComponent implements OnInit {

  paymentsForm!: FormGroup;
  formConfig: PaymentMethodConfig[] = [];
  selectedPaymentMethod!: number;
  submitted: boolean = false;
  @Input() paymentDetails!: PaymentDetails;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private sharedService: SharedService) { }

  ngOnInit(): void {
    this.sharedService.selectedPaymentMethod.subscribe(paymentMethod => this.selectedPaymentMethod = paymentMethod);
    this.http.get<PaymentMethodDetails>('http://localhost:4200/api/paymentMethodDetails/' + this.selectedPaymentMethod).subscribe(res => {
      this.formConfig = res.configuration;
    })
    this.paymentsForm = this.formBuilder.group({
      pan: ['', Validators.compose([Validators.required, this.luhnAlgorithmCheck()])],
      cvc: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern("^[0-9]*$")])],
      expDate: ['', Validators.compose([Validators.required, Validators.pattern(/^(0[1-9]|1[0-2])\/?([0-9]{2})$/)])],
      cardHolder: ['', Validators.compose([Validators.required, Validators.minLength(15), Validators.maxLength(50)])],
      payments: this.formBuilder.array([])
    })
  }

  luhnAlgorithmCheck(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let pan = control.value;
      if (/[^0-9-\s]+/.test(pan)) return { luhnAlgorithmCheckFail: { value: control.value } };

      var nCheck = 0, nDigit = 0, bEven = false;
      pan = pan.replace(/\D/g, "");

      for (var n = pan.length - 1; n >= 0; n--) {
        var cDigit = pan.charAt(n),
          nDigit = parseInt(cDigit, 10);

        if (bEven) {
          if ((nDigit *= 2) > 9) nDigit -= 9;
        }

        nCheck += nDigit;
        bEven = !bEven;
      }

      return !(nCheck % 10 == 0) ? { luhnAlgorithmCheckFail: { value: control.value } } : null;
    }
  }

  submit() {
    this.submitted = true;
    if (this.paymentsForm.valid) {

      this.http.get<Payment[]>('http://localhost:4200/api/payments').subscribe(res => {

        let payment = new Payment(res.length + 1, this.selectedPaymentMethod, this.paymentDetails.amount, this.paymentDetails.currency,
          this.paymentsForm.controls.iban?.value, this.paymentsForm.controls.bic?.value,
          this.paymentsForm.controls.holderName?.value, this.paymentsForm.controls.pan?.value,
          this.paymentsForm.controls.expDate?.value, this.paymentsForm.controls.cvc?.value, this.paymentsForm.controls.cardHolder?.value);

        this.http.post<Payment>('http://localhost:4200/api/payments', payment).subscribe(res => {
          this.http.get<Payment[]>('http://localhost:4200/api/payments').subscribe(res => {
            console.log(res);
            this.sharedService.changeSelectedPaymentMethod(3);
          })
        })
      })
    }
  }

  returnToPaymentSelect() {
    this.sharedService.changeSelectedPaymentMethod(0);
  }

}
