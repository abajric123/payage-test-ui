export class Payment {
    id: number;
    paymentMethodId: number;
    amount: number;
    currency: string;
    iban: string;
    bic: string;
    holderName: string;
    cardNumber: string;
    expDate: string;
    cvc: number;
    cardHolder: string;

    constructor(id: number, paymentMethodId: number, amount: number, currency: string, iban: string, bic: string, holderName: string,
        cardNumber: string, expDate: string, cvc: number, cardHolder: string) {
        this.id = id;
        this.paymentMethodId = paymentMethodId;
        this.amount = amount;
        this.currency = currency;
        this.iban = iban;
        this.bic = bic;
        this.holderName = holderName;
        this.cardNumber = cardNumber;
        this.expDate = expDate;
        this.cvc = cvc;
        this.cardHolder = cardHolder;
    }
}