import { PaymentMethod } from "./PaymentMethod";

export class PaymentDetails {
  amount: number;
  currency: string;
  methods: PaymentMethod[];

  constructor(amount: number, currency: string, methods: PaymentMethod[]) {
    this.amount = amount;
    this.currency = currency;
    this.methods = methods;
  }
}