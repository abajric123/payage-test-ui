import { PaymentMethodConfig } from "./PaymentMethodConfig";

export class PaymentMethodDetails {
    id: number;
    configuration: PaymentMethodConfig[];

    constructor(id: number, configuration: PaymentMethodConfig[]) {
        this.id = id;
        this.configuration = configuration;
    }
}