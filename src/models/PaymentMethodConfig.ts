export class PaymentMethodConfig {
    formControlName: string;
    label: string;
    type: string;
    validations: string[];

    constructor(formControlName: string, label: string, type: string, validations: string[]) {
        this.formControlName = formControlName;
        this.label = label;
        this.type = type;
        this.validations = validations;
    }
}