import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  paymentMethod = 0;

  selectedPaymentMethod: BehaviorSubject<number>;

  constructor() {
    this.selectedPaymentMethod = new BehaviorSubject(this.paymentMethod);
  }

  changeSelectedPaymentMethod(paymentMethod: number) {
    this.selectedPaymentMethod.next(paymentMethod);
  }

}
