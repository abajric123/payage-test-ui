import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';
import { Payment } from 'src/models/Payment';

@Injectable({
  providedIn: 'root'
})
export class InMemPaymentService implements InMemoryDbService {

  constructor() { }
  createDb() {
    let payments: Payment[] = [];

    let paymentDetails = {
      amount: 16.50,
      currency: 'USD',
      methods: [
        { id: 1, name: 'VISA' },
        { id: 2, name: 'SEPA' }
      ]
    };

    let paymentMethodDetails = [
      {
        id: 1, configuration: [
          {
            formControlName: 'pan',
            label: 'Card Number',
            type: 'String',
            validations: ['Validators.required', 'Luhn Algorithm Check']
          },
          {
            formControlName: 'cvc',
            label: 'CVC',
            type: 'Integer',
            validations: ['Validators.required', 'Validators.minLength(3)', 'Validators.maxLength(3)']
          },
          {
            formControlName: 'expDate',
            label: 'Expiration Date',
            type: 'String',
            validations: ['Validators.required', 'MM/YY format']
          },
          {
            formControlName: 'cardHolder',
            label: 'Card Holder',
            type: 'String',
            validations: ['Validators.required', 'Validators.minLength(15)', 'Validators.maxLength(50)']
          }
        ]
      },
      {
        id: 2, configuration: [
          {
            formControlName: 'iban',
            label: 'IBAN',
            type: 'String',
            validations: ['Validators.required', 'Alphanumeric Length 15-32']
          },
          {
            formControlName: 'bic',
            label: 'BIC',
            type: 'String',
            validations: ['Validators.required', 'Alphanumeric Length 8']
          },
          {
            formControlName: 'holderName',
            label: 'Holder name',
            type: 'String',
            validations: ['Validators.required', 'Length 15-50']
          }
        ]
      }
    ];


    return { paymentDetails, paymentMethodDetails, payments };
  }
}
