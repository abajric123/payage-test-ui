import { TestBed } from '@angular/core/testing';

import { InMemPaymentService } from './in-mem-payment.service';

describe('InMemPaymentService', () => {
  let service: InMemPaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InMemPaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
